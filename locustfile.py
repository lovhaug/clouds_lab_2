from locust import HttpUser, task


class NumericTester(HttpUser):
    @task
    def numeric_test(self):
        self.client.get('/integral/0/3.14159')
