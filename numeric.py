import numpy as np
from flask import Flask
from urllib import parse

app = Flask(__name__)


@app.route('/integral/<start>/<end>')
def numericalintegralservice(start, end):
    """
    Compute the integral of a function between two points.
    """
    start = float(parse.unquote(start))
    end = float(parse.unquote(end))
    compute_integral(start, end, 10)
    compute_integral(start, end, 100)
    compute_integral(start, end, 1000)
    compute_integral(start, end, 10_000)
    compute_integral(start, end, 100_000)
    return str(compute_integral(start, end, 1_000_000))


def compute_integral(start: float, end: float, subdivisions: int) -> float:
    """Compute the integral of a function between two points.

    >>> compute_integral(0, 10, 1000)
    0.49999999999999894
    """

    def func(x):
        return np.abs(np.sin(x))

    f = func

    x = np.linspace(start, end, subdivisions)
    y = f(x)
    dx = x[1] - x[0]
    return np.sum(y * dx)


if __name__ == '__main__':
    app.run(debug=True)
