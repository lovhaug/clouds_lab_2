from typing import Dict, List, Tuple
import azure.functions as func
import azure.durable_functions as df   # type: ignore
from azure.storage.blob import BlobServiceClient

myApp = df.DFApp(http_auth_level=func.AuthLevel.ANONYMOUS)

# An HTTP-Triggered Function with a Durable Functions Client binding
@myApp.route(route='orchestrators/{functionName}')
@myApp.durable_client_input(client_name='client')
async def http_start(req: func.HttpRequest, client):
    function_name = req.route_params.get('functionName')
    instance_id = await client.start_new(function_name)
    response = client.create_check_status_response(req, instance_id)
    return response


# Orchestrator
@myApp.orchestration_trigger(context_name='context')
def mapper_orchestrator(context):

    input_data = yield context.call_activity('get_input_data')

    tasks = []

    for line_number, line in enumerate(input_data):
        print(line)
        tasks.append(
            context.call_activity(
                'mapper', {'line_number': line_number, 'line': line}
            )
        )

    output = yield context.task_all(tasks)

    shuffled = yield context.call_activity('shuffle', output)

    reduced_tasks = []
    for word, count in shuffled.items():
        reduced_tasks.append(
            context.call_activity('reducer', {'word': word, 'count': count})
        )

    reduced = yield context.task_all(reduced_tasks)
    print(reduced)

    return reduced


@myApp.activity_trigger(input_name='inputdata')
def get_input_data(inputdata: List[str]):
    """Reads the four files found in Azure blob storage and returns a list of strings"""
    client = get_blob_service_client()
    container_client = client.get_container_client('mapreduce-input')
    files = container_client.list_blobs()
    result = []
    for file in files:
        result.append(read_text_file('mapreduce-input', file.name))
    result = flatten_extend(result)
    return result


def get_blob_service_client():
    connection_string = 'DefaultEndpointsProtocol=https;AccountName=lab2mapreducestorage;AccountKey=S/VfAPJA4oJgpJWexx9ln9LvDmAeRf+8Q2uGuoiGm1KqMTyzng3tx44Uu/hDBLim9NYfsQuj10La+AStUdRk1g==;EndpointSuffix=core.windows.net'
    return BlobServiceClient.from_connection_string(connection_string)


def read_text_file(container_name, blob_name):
    blob_service_client = get_blob_service_client()
    container_client = blob_service_client.get_container_client(container_name)
    blob_client = container_client.get_blob_client(blob_name)

    content = blob_client.download_blob().readall()
    content = content.decode('utf-8')
    content = content.replace('\r', '')
    content = content.split('\n')
    return content


@myApp.activity_trigger(input_name='mapdata')
def mapper(mapdata: Dict[str, str]):
    """Take in a dictionary with a 'line_number' key and a 'line' key"""
    result = []
    words = mapdata['line'].split(' ')
    for word in words:
        result.append((word, 1))
    return result


@myApp.activity_trigger(input_name='reducerdata')
def reducer(reducerdata: Dict[str, List[int]]):
    """Take in a dictionary with a 'word' key and a 'count' key"""
    return (reducerdata['word'], sum(reducerdata['count']))


@myApp.activity_trigger(input_name='shuffledata')
def shuffle(shuffledata: List[List[Tuple[str, int]]]):
    """Take a list of tuple and return a list of dictionary where the key is the word and the value is a list of counts"""
    flat_list = flatten_extend(shuffledata)
    output: Dict[str, List[int]] = {}
    for word in flat_list:
        if word[0] in output:
            output[word[0]].append(word[1])
        else:
            output[word[0]] = [word[1]]

    return output


def flatten_extend(matrix):
    flat_list = []
    for row in matrix:
        flat_list.extend(row)
    return flat_list
