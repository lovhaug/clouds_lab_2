import azure.functions as func
import numpy as np
import logging

app = func.FunctionApp()


@app.route(route='Integral/{start}/{end}', auth_level=func.AuthLevel.ANONYMOUS)
def Integral(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    start = req.route_params.get('start')
    end = req.route_params.get('end')

    if start and end:
        start = float(start)
        end = float(end)

        logging.info(f'start: {start}')
        logging.info(f'end: {end}')

        compute_integral(start, end, 10)
        compute_integral(start, end, 100)
        compute_integral(start, end, 1000)
        compute_integral(start, end, 10_000)
        compute_integral(start, end, 100_000)
        result = compute_integral(start, end, 1_000_000)

        return func.HttpResponse(f'result: {result}', status_code=200)
    else:
        return func.HttpResponse(
            'Please pass a start and end on the query string or in the request body',
            status_code=400,
        )


def compute_integral(start: float, end: float, subdivisions: int) -> float:
    """Compute the integral of a function between two points.

    >>> compute_integral(0, 10, 1000)
    0.49999999999999894
    """

    def func(x):
        return np.abs(np.sin(x))

    f = func

    x = np.linspace(start, end, subdivisions)
    y = f(x)
    dx = x[1] - x[0]
    return np.sum(y * dx)
